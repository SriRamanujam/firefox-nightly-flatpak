/*global pref*/
/*eslint no-undef: "error"*/
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 pref("intl.locale.requested", "");
 pref("app.update.auto", false);
 pref("app.update.enabled", false);
 pref("app.update.autoInstallEnabled", false);
 pref("browser.shell.checkDefaultBrowser", false);
 pref("spellchecker.dictionary_path", "/usr/share/hunspell");

 pref("gfx.webrender.all", true);
 pref("media.ffmpeg.low-latency.enabled", true);
 pref("media.ffmpeg.vaapi.enabled", true);
 pref("media.ffvpx.enabled", false);
 pref("media.rdd-ffvpx.enabled", false);
 pref("media.ffmpeg.low-latency.enabled", true);
 pref("layers.acceleration.force-enabled", true);
 pref("widget.wayland-dmabuf-vaapi.enabled", true);
 pref("widget.wayland-dmabuf-video-textures.enabled", true);

 pref("network.http.referer.trimmingPolicy", 1);
 pref("network.http.referer.XOriginTrimmingPolicy", 2);
 pref("privacy.firstparty.isolate", true);
 pref("fission.autostart", true);

 pref("widget.use-xdg-desktop-portal", true);
 pref("apz.allow_zooming_out", true);
 pref("image.avif.enabled", true);
 pref("image.jxl.enabled", true);